import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.utils import to_categorical
from keras.callbacks import Callback
import sys, random, math
from PySide6.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QGraphicsView, QSizePolicy, QGraphicsItem, \
    QSlider, QVBoxLayout, QWidget, QLabel, QPushButton, QGraphicsTextItem
from PySide6.QtCore import Qt, QSize, QPoint, Slot, QTimer, QPropertyAnimation
from PySide6.QtGui import QBrush, QPen, QTransform, QPainter, QFont

train_acc = []
val_acc = []
predicted_labels = []
digit_probabilities = []

class VisGraphicsScene(QGraphicsScene):
    def __init__(self):
        super(VisGraphicsScene, self).__init__()
        self.selection = None
        self.wasDragg = False
        self.pen = QPen(Qt.black)
        self.selected = QPen(Qt.red)

    def mouseReleaseEvent(self, event):
        if (self.wasDragg):
            return
        if (self.selection):
            self.selection.setPen(self.pen)
        item = self.itemAt(event.scenePos(), QTransform())
        if (item):
            item.setPen(self.selected)
            self.selection = item


class VisGraphicsView(QGraphicsView):
    def __init__(self, scene, parent):
        super(VisGraphicsView, self).__init__(scene, parent)
        self.startX = 0.0
        self.startY = 0.0
        self.distance = 0.0
        self.myScene = scene
        self.setRenderHints(QPainter.Antialiasing | QPainter.TextAntialiasing | QPainter.SmoothPixmapTransform)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorViewCenter)

    def wheelEvent(self, event):
        zoom = 1 + event.angleDelta().y() * 0.001;
        self.scale(zoom, zoom)

    def mousePressEvent(self, event):
        self.startX = event.pos().x()
        self.startY = event.pos().y()
        self.myScene.wasDragg = False
        super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        endX = event.pos().x()
        endY = event.pos().y()
        deltaX = endX - self.startX
        deltaY = endY - self.startY
        distance = math.sqrt(deltaX * deltaX + deltaY * deltaY)
        if (distance > 5):
            self.myScene.wasDragg = True
        super().mouseReleaseEvent(event)


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('MNIST Visualisation')
        self.createGraphicView()
        self.createToolbar()  # Add the toolbar with play button
        self.playing = False  # Flag to indicate if animation is playing
        self.current_epoch = 0  # Current epoch index
        self.play_timer = QTimer()  # Timer for animation
        self.play_timer.timeout.connect(self.updateEpoch)  # Connect timeout signal to updateEpoch method
        self.slider_animation = QPropertyAnimation(self, b"sliderPosition")  # Animation for slider position
        self.show()

    def createToolbar(self):
        toolbar = self.addToolBar('Slider')
        toolbar.setMovable(False)

        self.play_button = QPushButton("Play")
        self.play_button.setCheckable(True)
        self.play_button.clicked.connect(self.togglePlay)  # Connect button click signal to togglePlay method

        toolbar.addWidget(self.play_button)

        self.label = QLabel('Epoch: ')
        toolbar.addWidget(self.label)

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(1)
        self.slider.setMaximum(10)
        self.slider.setValue(1)
        self.slider.setTickInterval(1)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.valueChanged.connect(self.sliderValueChanged)
        toolbar.addWidget(self.slider)

    def togglePlay(self):
        if self.playing:
            self.stopAnimation()
        else:
            self.startAnimation()

    def startAnimation(self):
        self.playing = True
        self.play_button.setText("Pause")
        self.slider_animation.setStartValue(self.slider.value())
        self.slider_animation.setEndValue(self.slider.maximum())
        self.slider_animation.start()
        self.play_timer.start(1000)  # Set the timer interval to 1000 ms (1 second)
        self.current_epoch = 0  # Reset current_epoch to 0

    def stopAnimation(self):
        print("anim stopped")
        print(self.current_epoch)
        self.playing = False
        self.play_button.setText("Play")
        self.slider_animation.stop()
        self.play_timer.stop()

    def updateEpoch(self):
        if self.current_epoch < len(predicted_labels):
            # Update the displayed glyph based on the current epoch
            glyph_probabilities = predicted_labels[self.current_epoch]
            # Update the graphics scene with the new probabilities or glyph data
            self.sliderValueChanged(self.current_epoch)
            self.set_sliderPosition(self.current_epoch)
            # Increment the current epoch
            self.current_epoch += 1
        else:
            # Animation reached the end, stop playing only if not paused
            if not self.playing:
                self.stopAnimation()
            else:
                self.sliderValueChanged(self.current_epoch)
                self.set_sliderPosition(self.current_epoch)
                self.current_epoch = 0

    def sliderValueChanged(self, value):
        print(value)
        glyph_index = value - 1
        if glyph_index < len(predicted_labels):
            glyph_probabilities = predicted_labels[glyph_index]

    def get_sliderPosition(self):
        return self.slider.value()

    def set_sliderPosition(self, value):
        self.slider.setValue(value)

    sliderPosition = property(get_sliderPosition, set_sliderPosition)

    def createGraphicView(self):
        self.scene = VisGraphicsScene()
        self.pen = QPen(Qt.black)

        # Create a decagram with 10 points
        num_points = 10
        radius_outer = 100
        radius_inner = radius_outer * math.cos(math.pi / num_points)
        center = QPoint(400, 300)  # Adjust the center coordinates as needed

        angle_increment = 360 / num_points
        start_angle = 90  # Starting angle (top)
        points = []

        # Generate the points of the decagram
        for i in range(num_points):
            angle_rad = math.radians(start_angle + i * angle_increment)
            if i % 2 == 0:
                x = center.x() + radius_outer * math.cos(angle_rad)
                y = center.y() - radius_outer * math.sin(angle_rad)
            else:
                x = center.x() + radius_inner * math.cos(angle_rad)
                y = center.y() - radius_inner * math.sin(angle_rad)
            points.append(QPoint(x, y))

        # Define different colors for each point
        colors = [Qt.red, Qt.green, Qt.blue, Qt.yellow, Qt.cyan, Qt.magenta, Qt.gray, Qt.darkRed, Qt.darkGreen,
                  Qt.darkBlue]

        start_angle = 90  # Starting angle (top)
        # Add the points to the scene with different colors
        for i, point in enumerate(points):
            # Add grey line from center to each point
            self.scene.addLine(center.x(), center.y(), point.x(), point.y(), QPen(Qt.lightGray))

            # Add point with color
            item = self.scene.addEllipse(point.x() - 5, point.y() - 5, 10, 10, self.pen, QBrush(colors[i % len(colors)]))
            item.setFlag(QGraphicsItem.ItemIsSelectable)

            # Add label to the point
            label = self.scene.addText(str(i), QFont())
            label.setDefaultTextColor(Qt.black)
            label.setScale(1.5)  # Increase label size

            # Position the label at a consistent distance from the point
            label_width = label.boundingRect().width()
            label_height = label.boundingRect().height()

            # Determine the position of the point
            angle_rad = math.radians(start_angle + i * angle_increment)
            if i % 2 == 0:
                x = center.x() + (radius_outer+20) * math.cos(angle_rad)
                y = center.y() - (radius_outer+20) * math.sin(angle_rad)
            else:
                x = center.x() + (radius_inner+20) * math.cos(angle_rad)
                y = center.y() - (radius_inner+20) * math.sin(angle_rad)

            label.setPos(x-(label_width * 0.75), y-(label_height * 0.75))

        self.view = VisGraphicsView(self.scene, self)
        self.setCentralWidget(self.view)
        self.view.setGeometry(0, 0, 800, 600)

class SavePredictionCheckpoint(Callback):
    def __init__(self, model, X_test, y_test, digit_counts):
        super(SavePredictionCheckpoint, self).__init__()
        self.model = model
        self.X_test = X_test
        self.y_test = y_test
        self.digit_counts = digit_counts
        self.predicted_labels = []

    def on_epoch_end(self, epoch, logs=None):
        # Predict labels for the input data
        predicted_labels = self.model.predict(self.X_test)  # Assuming X_train contains the input images

        # Append the predicted labels to the list
        self.predicted_labels.append(predicted_labels)


def train():
    # Load the MNIST dataset
    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    # Reshape the input data and normalize it
    X_train = X_train.reshape(60000, 784).astype('float32') / 255
    X_test = X_test.reshape(10000, 784).astype('float32') / 255

    # Convert the labels to one-hot encoded vectors
    nb_classes = 10
    Y_train = to_categorical(y_train, nb_classes)
    Y_test = to_categorical(y_test, nb_classes)

    # Define the model architecture
    model = Sequential()
    model.add(Dense(512, input_shape=(784,)))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(10))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Calculate the number of occurrences of each digit in the dataset
    digit_counts = np.bincount(y_test)

    # Checkpoint during learning
    checkpoint = SavePredictionCheckpoint(model, X_test, y_test, digit_counts)

    # Train the model
    history = model.fit(X_train, Y_train, batch_size=128, epochs=10, verbose=1,
                        validation_data=(X_test, Y_test), callbacks=[checkpoint])

    # Extract train and validation accuracy
    train_acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    # Get the predicted labels
    predicted_labels = checkpoint.predicted_labels

    # Write output to the file
    with open("output.txt", 'w') as file:
        file.write("Train Accuracy:\n")
        file.write('\n'.join(str(acc) for acc in train_acc))
        file.write('\n\n')

        file.write("Validation Accuracy:\n")
        file.write('\n'.join(str(acc) for acc in val_acc))
        file.write('\n\n')

        file.write("Predicted Labels:\n")
        for epoch, labels in enumerate(predicted_labels):
            file.write(f"Epoch {epoch + 1}:\n")
            for digit in range(10):
                digit_sum = np.sum(labels[y_test == digit], axis=0)
                digit_average = digit_sum / digit_counts[digit]
                file.write(f"Digit {digit}: {', '.join(f'{value:.8f}' for value in digit_average)}\n")
            file.write('\n')

    print("Output written to output.txt")

def load_trained_data(input_file):

    with open(input_file, 'r') as file:
        lines = file.readlines()
        train_idx = lines.index("Train Accuracy:\n")
        val_idx = lines.index("Validation Accuracy:\n")
        labels_idx = lines.index("Predicted Labels:\n")

        for i in range(train_idx + 1, val_idx):
            line = lines[i].strip()
            if line:
                acc = float(line)
                train_acc.append(acc)

        for i in range(val_idx + 1, labels_idx):
            line = lines[i].strip()
            if line:
                acc = float(line)
                val_acc.append(acc)

        epoch_start_idx = labels_idx + 1
        num_epochs = lines.count("Epoch ")
        current_epoch = None
        current_labels = {}

        for i in range(epoch_start_idx, len(lines)):
            line = lines[i].strip()
            if line.startswith("Epoch "):
                if current_epoch is not None:
                    predicted_labels.append(current_labels)
                current_epoch = line.split("Epoch ")[1].rstrip(":")
                current_labels = {}
            elif line.startswith("Digit "):
                parts = line.split(":")
                digit = int(parts[0].split("Digit ")[1].strip())
                probabilities = [float(value.strip()) for value in parts[1].split(",")]
                current_labels[digit] = probabilities

        # Add the last epoch's labels
        if current_epoch is not None:
            predicted_labels.append(current_labels)



    # PRINT THOSE FUCKERS
    print("Train Accuracy:")
    for acc in train_acc:
        print(acc)


    print("Validation Accuracy:")
    for acc in val_acc:
        print(acc)

    print("Predicted Labels:")
    for epoch, labels in enumerate(predicted_labels):
        print(f"Epoch {epoch + 1}:")
        for digit in range(10):
            digit_average = labels[digit]
            print(f"Digit {digit}: {', '.join(f'{value:.8f}' for value in digit_average)}")
        print()


# Start training
app = QApplication(sys.argv)
#train()
load_trained_data("output.txt")


# Create the main window
main_window = MainWindow()
main_window.show()

sys.exit(app.exec())