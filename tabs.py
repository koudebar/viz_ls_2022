import sys
import random
import math
import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')  # Use Qt5Agg backend
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QComboBox
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.utils import to_categorical

train_acc = []
val_acc = []

class CustomFigureCanvas(FigureCanvas):
    def __init__(self, figure):
        super().__init__(figure)
        self.startX = 0.0
        self.startY = 0.0
        self.distance = 0.0

    def wheelEvent(self, event):
        zoom = 1 + event.angleDelta().y() * 0.001
        self.figure.axes[0].scale(zoom, zoom)
        self.draw()

    def mousePressEvent(self, event):
        self.startX = event.pos().x()
        self.startY = event.pos().y()

    def mouseReleaseEvent(self, event):
        endX = event.pos().x()
        endY = event.pos().y()
        deltaX = endX - self.startX
        deltaY = endY - self.startY
        distance = math.sqrt(deltaX * deltaX + deltaY * deltaY)
        if distance > 5:
            self.figure.axes[0].pan_delta(-deltaX, -deltaY)
            self.draw()


class MainWindow(QMainWindow):
    def __init__(self, train_acc=None, val_acc=None):
        super().__init__()
        self.setWindowTitle("Training and validation accuracy")

        # Create a Matplotlib figure for each canvas
        self.figures = [Figure(figsize=(5, 5), dpi=100) for _ in range(3)]

        # Create custom canvases for each figure
        self.canvases = [CustomFigureCanvas(figure) for figure in self.figures]

        # Create the Matplotlib axes for each canvas
        self.axes = [figure.add_subplot(111) for figure in self.figures]

        # Create a combo box to switch between canvases
        self.canvas_combo = QComboBox()
        self.canvas_combo.addItem("Canvas 1")
        self.canvas_combo.addItem("Canvas 2")
        self.canvas_combo.addItem("Canvas 3")
        self.canvas_combo.currentIndexChanged.connect(self.switch_canvas)

        # Create a navigation toolbar
        self.toolbar = NavigationToolbar(self.canvases[0], self)

        # Create a layout and add the toolbar, combo box, and current canvas
        layout = QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas_combo)
        layout.addWidget(self.canvases[0])

        # Create a central widget and set the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)

        # Set the central widget of the main window
        self.setCentralWidget(central_widget)

    def switch_canvas(self, index):
        # Remove the previous canvas and toolbar from the layout
        layout = self.centralWidget().layout()
        layout.removeWidget(self.toolbar)
        layout.removeWidget(self.canvases[0])

        # Add the selected canvas and toolbar to the layout
        self.toolbar = NavigationToolbar(self.canvases[index], self)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvases[index])

    def plot_accuracy(self):
        # Plot the training and validation accuracy values for each epoch
        epochs = range(1, len(train_acc) + 1)
        self.axes[0].plot(epochs, train_acc, 'bo', label='Training accuracy')
        self.axes[0].plot(epochs, val_acc, 'b', label='Validation accuracy')
        self.axes[0].set_title('Training and validation accuracy')
        self.axes[0].set_xlabel('Epochs')
        self.axes[0].set_ylabel('Accuracy')
        self.axes[0].legend()
        self.canvases[0].draw()

        # Add placeholder plots for the other canvases
        for i in range(1, 3):
            self.axes[i].plot([], [])
            self.canvases[i].draw()


def train():
    # Load the MNIST dataset
    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    # Reshape the input data and normalize it
    X_train = X_train.reshape(60000, 784).astype('float32') / 255
    X_test = X_test.reshape(10000, 784).astype('float32') / 255

    # Convert the labels to one-hot encoded vectors
    nb_classes = 10
    Y_train = to_categorical(y_train, nb_classes)
    Y_test = to_categorical(y_test, nb_classes)

    # Define the model architecture
    model = Sequential()
    model.add(Dense(512, input_shape=(784,)))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(10))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Train the model and collect the training and validation accuracy values for each epoch
    history = model.fit(X_train, Y_train, batch_size=128, epochs=10, verbose=1, validation_data=(X_test, Y_test))
    train_acc.extend(history.history['accuracy'])
    val_acc.extend(history.history['val_accuracy'])

    # Print out the accuracy values
    with open("output.txt", 'w') as file:
        file.write("Train Accuracy:\n")
        file.write('\n'.join(str(acc) for acc in train_acc))
        file.write('\n\n')  # Add a separate line between train_acc and val_acc

        # Write val_acc to the file
        file.write("Validation Accuracy:\n")
        file.write('\n'.join(str(acc) for acc in val_acc))


def load_trained_data(input_file):
    with open(input_file, 'r') as file:
        lines = file.readlines()
        train_idx = lines.index("Train Accuracy:\n")
        val_idx = lines.index("Validation Accuracy:\n")

        for i in range(train_idx + 1, val_idx):
            line = lines[i].strip()
            if line:
                acc = float(line)
                train_acc.append(acc)

        for i in range(val_idx + 1, len(lines)):
            line = lines[i].strip()
            if line:
                acc = float(line)
                val_acc.append(acc)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    # train()
    load_trained_data("output.txt")

    # Create the main window
    main_window = MainWindow(train_acc, val_acc)
    main_window.plot_accuracy()
    main_window.show()

    sys.exit(app.exec_())
