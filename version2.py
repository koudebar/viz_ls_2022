import PySide6
import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.utils import to_categorical
from keras.callbacks import Callback
import sys, math
from PySide6.QtCore import Qt, QPoint, QTimer, QPropertyAnimation
from PySide6.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QGraphicsView, QSizePolicy, QGraphicsItem, \
    QGraphicsPolygonItem, QVBoxLayout, QTableWidget, QSlider, QTableWidgetItem, QWidget, QLabel, QPushButton, QCheckBox, \
    QHBoxLayout
from PySide6.QtGui import QBrush, QPen, QTransform, QPainter, QFont, QPolygonF
from qtpy import QtCharts

train_acc = []
interval_radia = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
val_acc = []
predicted_labels = []
digit_probabilities = []
classification = []
globEpoch = 0

class VisGraphicsScene(QGraphicsScene):
    def __init__(self):
        super(VisGraphicsScene, self).__init__()
        self.selection = None
        self.wasDragg = False
        self.pen = QPen(Qt.lightGray)
        self.selected = QPen(Qt.red)

    def mouseReleaseEvent(self, event):
        if (self.wasDragg):
            return
        if (self.selection):
            self.selection.setPen(self.pen)
        item = self.itemAt(event.scenePos(), QTransform())
        if (item):
            item.setPen(self.selected)
            self.selection = item


class VisGraphicsView(QGraphicsView):
    def __init__(self, scene, parent):
        super(VisGraphicsView, self).__init__(scene, parent)
        self.startX = 0.0
        self.startY = 0.0
        self.distance = 0.0
        self.myScene = scene
        self.setRenderHints(QPainter.Antialiasing | QPainter.TextAntialiasing | QPainter.SmoothPixmapTransform)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorViewCenter)

    def wheelEvent(self, event):
        zoom = 1 + event.angleDelta().y() * 0.001;
        self.scale(zoom, zoom)

    def mousePressEvent(self, event):
        self.startX = event.pos().x()
        self.startY = event.pos().y()
        self.myScene.wasDragg = False
        super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        endX = event.pos().x()
        endY = event.pos().y()
        deltaX = endX - self.startX
        deltaY = endY - self.startY
        distance = math.sqrt(deltaX * deltaX + deltaY * deltaY)
        if (distance > 5):
            self.myScene.wasDragg = True
        super().mouseReleaseEvent(event)


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('MNIST Visualisation')
        self.current_epoch = 0
        self.glyph_epoch = 0
        self.checked_bools = [True] * 10
        self.createGraphicView()
        self.createToolbar()  # Add the toolbar with play button
        self.playing = False  # Flag to indicate if animation is playing
        self.play_timer = QTimer()  # Timer for animation
        self.play_timer.timeout.connect(self.updateEpoch)  # Connect timeout signal to updateEpoch method
        self.slider_animation = QPropertyAnimation(self, b"sliderPosition")  # Animation for slider position
        self.show()


    def createToolbar(self):
        toolbar = self.addToolBar('Slider')
        toolbar.setMovable(False)

        self.play_button = QPushButton("Play")
        self.play_button.setCheckable(True)
        self.play_button.clicked.connect(self.togglePlay)  # Connect button click signal to togglePlay method

        toolbar.addWidget(self.play_button)

        self.label = QLabel('Epoch: ')
        toolbar.addWidget(self.label)

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(1)
        self.slider.setMaximum(10)
        self.slider.setValue(1)
        self.slider.setTickInterval(1)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.valueChanged.connect(self.sliderValueChanged)
        toolbar.addWidget(self.slider)

    def togglePlay(self):
        if self.playing:
            self.stopAnimation()
        else:
            self.startAnimation()

    def startAnimation(self):
        self.playing = True
        self.play_button.setText("Pause")
        self.slider_animation.setStartValue(self.slider.value())
        self.slider_animation.setEndValue(self.slider.maximum())
        self.slider_animation.start()
        self.play_timer.start(1000)  # Set the timer interval to 1000 ms (1 second)
        self.current_epoch = 0  # Reset current_epoch to 0

    def stopAnimation(self):
        print("anim stopped")
        print(self.current_epoch)
        self.playing = False
        self.play_button.setText("Play")
        self.slider_animation.stop()
        self.play_timer.stop()


    def updateEpoch(self):
        if self.current_epoch < len(predicted_labels):
            # Update the displayed glyph based on the current epoch
            glyph_probabilities = predicted_labels[self.current_epoch]
            # Update the graphics scene with the new probabilities or glyph data
            self.sliderValueChanged(self.current_epoch)
            self.set_sliderPosition(self.current_epoch)
            # Increment the current epoch
            self.current_epoch += 1
        else:
            # Animation reached the end, stop playing only if not paused
            if not self.playing:
                self.stopAnimation()
            else:
                self.sliderValueChanged(self.current_epoch)
                self.set_sliderPosition(self.current_epoch)
                self.current_epoch = 0


    def sliderValueChanged(self, value):
        print(value-1)
        self.upd(value-1)

    def get_sliderPosition(self):
        return self.slider.value()

    def set_sliderPosition(self, value):
        self.slider.setValue(value)

    sliderPosition = property(get_sliderPosition, set_sliderPosition)

    def updateCheckbox(self, table, row, state):
        print("TUSEM")
        print(self.glyph_epoch, " ", row, " ", state, " ", Qt.Checked)
        if state == 2:
            print("checked")
            self.checked_bools[row] = True
            self.upd(self.glyph_epoch)
        else:
            print("unchecked")
            self.checked_bools[row] = False
            self.upd(self.glyph_epoch)

        print(self.checked_bools)

    def upd(self, value):
        self.glyph_epoch = value

        for item in self.scene.items():
            if type(item) is QGraphicsPolygonItem:
                self.scene.removeItem(item)
                # print(str(item) + "\n")

        num_points = 10
        radius_outer = 100
        radius_inner = radius_outer * math.cos(math.pi / num_points)
        angle_increment = 360 / num_points
        # Define different colors for each point
        colors = [Qt.red, Qt.green, Qt.blue, Qt.yellow, Qt.cyan, Qt.magenta, Qt.gray, Qt.darkRed, Qt.darkGreen,
                  Qt.darkBlue]

        # generate the Number glyphs for a specific epoch
        for j in range(num_points):
            if(self.checked_bools[j] is False): continue
            start_angle = 90
            # Generate the points for the star glyph
            pointPred = predicted_labels[value][j]
            pointCoor = []
            for i in range(num_points):
                tempCoef = 1
                # print(pointPred)
                for l in range(9, classification[j][i] - 1, -1):
                    if (l >= classification[j][i]):
                        tempCoef *= (interval_radia[l] / ((l + 1) * 0.1))
                        # if l==0:
                        # print("interval radia " + str(interval_radia[l]) + "temp " + str(((l + 1) * 0.1)))

                angle_rad = math.radians(start_angle + i * angle_increment)
                # Apply the interval scale
                if i % 2 == 0:
                    x = 400 + radius_outer * tempCoef * pointPred[i] * math.cos(angle_rad)
                    if x > 400 + radius_outer * 1 * math.cos(angle_rad) and math.cos(angle_rad) > 0:
                        x = 400 + radius_outer * 1 * math.cos(angle_rad)
                    elif x < 400 + radius_outer * 1 * math.cos(angle_rad) and math.cos(angle_rad) < 0:
                        x = 400 + radius_outer * 1 * math.cos(angle_rad)
                    y = 300 - radius_outer * tempCoef * pointPred[i] * math.sin(angle_rad)
                    if y > 300 - radius_outer * 1 * math.sin(angle_rad) and math.sin(angle_rad) < 0:
                        y = 300 - radius_outer * 1 * math.sin(angle_rad)
                    elif y < 300 - radius_outer * 1 * math.sin(angle_rad) and math.sin(angle_rad) > 0:
                        y = 300 - radius_outer * 1 * math.sin(angle_rad)
                else:
                    x = 400 + radius_inner * tempCoef * pointPred[i] * math.cos(angle_rad)
                    if x < 400 + radius_inner * 1 * math.cos(angle_rad) and math.cos(angle_rad) < 0:
                        x = 400 + radius_inner * 1 * math.cos(angle_rad)
                    elif x > 400 + radius_inner * 1 * math.cos(angle_rad) and math.cos(angle_rad) > 0:
                        x = 400 + radius_inner * 1 * math.cos(angle_rad)
                    y = 300 - radius_inner * tempCoef * pointPred[i] * math.sin(angle_rad)
                    if y < 300 - radius_inner * 1 * math.sin(angle_rad) and math.sin(angle_rad) > 0:
                        y = 300 - radius_inner * 1 * math.sin(angle_rad)
                    elif y > 300 - radius_inner * 1 * math.sin(angle_rad) and math.sin(angle_rad) < 0:
                        y = 300 - radius_inner * 1 * math.sin(angle_rad)
                pointCoor.append(QPoint(x, y))
            # Add the star glyph to the scene

            # for item in self.scene.items():
            # if type(item) is QGraphicsPolygonItem:
            # print(str(item) + "\n")

            polygon = QGraphicsPolygonItem(QPolygonF(pointCoor))
            polygon.setPen(QPen(colors[j]))
            self.scene.addItem(polygon)


    def createGraphicView(self):
        self.scene = VisGraphicsScene()
        self.pen = QPen(Qt.lightGray)

        # Create a decagram with 10 points
        num_points = 10
        radius_outer = 100
        radius_inner = radius_outer * math.cos(math.pi / num_points)
        center = QPoint(400, 300)  # Adjust the center coordinates as needed

        self.checkboxes = []

        for i in range(num_points):
            temp = []
            for j in range(num_points):
                temp.append(math.floor(predicted_labels[0][i][j] * 10))
            classification.append(temp)

        print("classified \n")
        print(classification)

        angle_increment = 360 / num_points
        start_angle = 90  # Starting angle (top)
        points = []

        # Generate the points of the decagram
        for i in range(num_points):
            angle_rad = math.radians(start_angle + i * angle_increment)
            if i % 2 == 0:
                x = center.x() + radius_outer * math.cos(angle_rad)
                y = center.y() - radius_outer * math.sin(angle_rad)
            else:
                x = center.x() + radius_inner * 1.05 * math.cos(angle_rad)
                y = center.y() - radius_inner * 1.05 * math.sin(angle_rad)
            points.append(QPoint(x, y))

        # Define different colors for each point
        colors = [Qt.red, Qt.green, Qt.blue, Qt.yellow, Qt.cyan, Qt.magenta, Qt.gray, Qt.darkRed, Qt.darkGreen,
                  Qt.darkBlue]

        start_angle = 90  # Starting angle (top)
        # Add the points to the scene with different colors
        for i, point in enumerate(points):
            # Add grey line from center to each point
            self.scene.addLine(center.x(), center.y(), point.x(), point.y(), QPen(Qt.lightGray))

            # Add point with color
            item = self.scene.addEllipse(point.x() - 5, point.y() - 5, 10, 10, self.pen, QBrush(colors[i % len(colors)]))
            item.setFlag(QGraphicsItem.ItemIsSelectable)

            # Add label to the point
            label = self.scene.addText(str(i), QFont())
            label.setDefaultTextColor(Qt.black)
            label.setScale(1.5)  # Increase label size

            # Position the label at a consistent distance from the point
            label_width = label.boundingRect().width()
            label_height = label.boundingRect().height()

            # Determine the position of the point
            angle_rad = math.radians(start_angle + i * angle_increment)
            if i % 2 == 0:
                x = center.x() + (radius_outer+20) * math.cos(angle_rad)
                y = center.y() - (radius_outer+20) * math.sin(angle_rad)
            else:
                x = center.x() + (radius_inner+20) * math.cos(angle_rad)
                y = center.y() - (radius_inner+20) * math.sin(angle_rad)

            label.setPos(x-(label_width * 0.75), y-(label_height * 0.75))

            # Define the interval circles
            interval_circles = []
            interval_radius = 100

            for j in interval_radia[::-1]:
                # Add interval circle with color
                circle = self.scene.addEllipse(center.x() - interval_radius * j, center.y() - interval_radius * j,
                                               interval_radius * j * 2, interval_radius * j * 2, self.pen)
                circle.setFlag(QGraphicsItem.ItemIsSelectable)
                circle.setAcceptHoverEvents(True)
                interval_circles.append(circle)

        self.view = VisGraphicsView(self.scene, self)
        self.setCentralWidget(self.view)
        self.view.setGeometry(0, 0, 800, 600)

        # Create a QVBoxLayout to hold the graph view and table
        layout = QVBoxLayout()

        # Add the graph view to the layout
        layout.addWidget(self.view)

        checkboxes_layout = QHBoxLayout()

        for i in range(num_points):
            # Create the checkbox
            checkbox = QCheckBox()
            checkbox.setChecked(True)
            # Append the checkbox to the list
            self.checkboxes.append(checkbox)

            # Create the label for the checkbox
            label = QLabel(str(i))

            print(i)
            # Add the checkbox and label to the checkboxes layout
            checkboxes_layout.addWidget(checkbox)
            checkboxes_layout.addWidget(label)

            # Connect the checkbox's stateChanged signal to update the corresponding value in the table
            checkbox.stateChanged.connect(lambda state, row=i: self.updateCheckbox(0, row, state))

         # Create a QWidget to hold the checkboxes layout
        checkboxes_widget = QWidget()
        checkboxes_widget.setLayout(checkboxes_layout)

        # Add the checkboxes widget to the layout
        layout.addWidget(checkboxes_widget)

        layout2 = QHBoxLayout()
        # Create a QTableWidget for the sliders table
        table = QTableWidget()

        # Add columns to the table
        table.setColumnCount(2)
        table.setHorizontalHeaderLabels(['Change interval', 'Value'])

        # Add rows to the table based on the number of sliders
        num_sliders = 10  # Adjust the number of sliders as needed
        table.setRowCount(num_sliders)


        # Populate the table with sliders and their values
        for i in range(num_sliders):
            slider = QSlider(Qt.Horizontal)
            slider.setMinimum(0)
            slider.setMaximum(10)
            slider.setValue(interval_radia[i]*10)
            table.setCellWidget(i, 0, slider)

            # Connect the slider's valueChanged signal to update the corresponding value in the table
            slider.valueChanged.connect(lambda value, row=i: self.update_value(table, value/10, row))

            # Update the initial value in the table
            self.update_value(table, slider.value()/10, i)

        # Add the table to the layout
        layout2.addWidget(table, 0.75)

        # Create a QWidget to hold the layout
        widget = QWidget()
        widget.setLayout(layout)

        # Create a QtChart line series for the learning curve
        learning_curve_series = QtCharts.QLineSeries()

        # Add data points to the learning curve series
        for epoch, loss in enumerate(train_acc):
            learning_curve_series.append(epoch, loss)

        # Create a QtChart line series for the learning curve
        testing_curve_series = QtCharts.QLineSeries()

        # Add data points to the learning curve series
        for epoch, loss in enumerate(val_acc):
            testing_curve_series.append(epoch, loss)

        epoch_series = QtCharts.QLineSeries()

        for i in range(num_points):
            epoch_series.append(i, i)

        # Create a QtChart chart view for the learning curve
        chart_view = QtCharts.QChartView()
        learning_curve_series.setName("Learning Curve")
        testing_curve_series.setName("Testing Curve")
        chart_view.chart().addSeries(learning_curve_series)
        chart_view.chart().addSeries(testing_curve_series)

        axis_x = QtCharts.QValueAxis()
        axis_x.setLabelFormat("%d")
        axis_x.setTitleText("Epoch")
        chart_view.chart().addAxis(axis_x, Qt.AlignBottom)
        learning_curve_series.attachAxis(axis_x)
        testing_curve_series.attachAxis(axis_x)

        axis_y = QtCharts.QValueAxis()
        axis_y.setLabelFormat("%.2f")
        axis_y.setTitleText("Loss")
        chart_view.chart().addAxis(axis_y, Qt.AlignLeft)
        learning_curve_series.attachAxis(axis_y)
        testing_curve_series.attachAxis(axis_y)

        # Add the chart view to the layout
        layout2.addWidget(chart_view, 1)

        temp = QWidget()
        temp.setLayout(layout2)

        layout.addWidget(temp, 0.98)

        # Set the QWidget as the central widget of the main window
        self.setCentralWidget(widget)

    # Define a function to update the value in the table and polygons when a slider is moved
    def update_value(self, table, slider_value, row):
        value_item = QTableWidgetItem(str(slider_value))
        interval_radia[row] = slider_value
        # print("radius " + str(row) + "changed to " + str(interval_radia[row]))
        table.setItem(row, 1, value_item)
        self.upd(self.glyph_epoch)
        print(self.glyph_epoch)


class SavePredictionCheckpoint(Callback):
    def __init__(self, model, X_test, y_test, digit_counts):
        super(SavePredictionCheckpoint, self).__init__()
        self.model = model
        self.X_test = X_test
        self.y_test = y_test
        self.digit_counts = digit_counts
        self.predicted_labels = []

    def on_epoch_end(self, epoch, logs=None):
        # Predict labels for the input data
        predicted_labels = self.model.predict(self.X_test)  # Assuming X_train contains the input images

        # Append the predicted labels to the list
        self.predicted_labels.append(predicted_labels)

def train():
    # Load the MNIST dataset
    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    # Reshape the input data and normalize it
    X_train = X_train.reshape(60000, 784).astype('float32') / 255
    X_test = X_test.reshape(10000, 784).astype('float32') / 255

    # Convert the labels to one-hot encoded vectors
    nb_classes = 10
    Y_train = to_categorical(y_train, nb_classes)
    Y_test = to_categorical(y_test, nb_classes)

    # Define the model architecture
    model = Sequential()
    model.add(Dense(512, input_shape=(784,)))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(10))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Calculate the number of occurrences of each digit in the dataset
    digit_counts = np.bincount(y_test)

    # Checkpoint during learning
    checkpoint = SavePredictionCheckpoint(model, X_test, y_test, digit_counts)

    # Train the model
    history = model.fit(X_train, Y_train, batch_size=128, epochs=10, verbose=1,
                        validation_data=(X_test, Y_test), callbacks=[checkpoint])

    # Extract train and validation accuracy
    train_acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    # Get the predicted labels
    predicted_labels = checkpoint.predicted_labels

    # Write output to the file
    with open("output.txt", 'w') as file:
        file.write("Train Accuracy:\n")
        file.write('\n'.join(str(acc) for acc in train_acc))
        file.write('\n\n')

        file.write("Validation Accuracy:\n")
        file.write('\n'.join(str(acc) for acc in val_acc))
        file.write('\n\n')

        file.write("Predicted Labels:\n")
        for epoch, labels in enumerate(predicted_labels):
            file.write(f"Epoch {epoch + 1}:\n")
            for digit in range(10):
                digit_sum = np.sum(labels[y_test == digit], axis=0)
                digit_average = digit_sum / digit_counts[digit]
                file.write(f"Digit {digit}: {', '.join(f'{value:.8f}' for value in digit_average)}\n")
            file.write('\n')

    print("Output written to output.txt")

def load_trained_data(input_file):

    with open(input_file, 'r') as file:
        lines = file.readlines()
        train_idx = lines.index("Train Accuracy:\n")
        val_idx = lines.index("Validation Accuracy:\n")
        labels_idx = lines.index("Predicted Labels:\n")

        for i in range(train_idx + 1, val_idx):
            line = lines[i].strip()
            if line:
                acc = float(line)
                train_acc.append(acc)

        for i in range(val_idx + 1, labels_idx):
            line = lines[i].strip()
            if line:
                acc = float(line)
                val_acc.append(acc)

        epoch_start_idx = labels_idx + 1
        num_epochs = lines.count("Epoch ")
        current_epoch = None
        current_labels = {}

        for i in range(epoch_start_idx, len(lines)):
            line = lines[i].strip()
            if line.startswith("Epoch "):
                if current_epoch is not None:
                    predicted_labels.append(current_labels)
                current_epoch = line.split("Epoch ")[1].rstrip(":")
                current_labels = {}
            elif line.startswith("Digit "):
                parts = line.split(":")
                digit = int(parts[0].split("Digit ")[1].strip())
                probabilities = [float(value.strip()) for value in parts[1].split(",")]
                current_labels[digit] = probabilities

        # Add the last epoch's labels
        if current_epoch is not None:
            predicted_labels.append(current_labels)



    # PRINT THOSE FUCKERS
    #print("Train Accuracy:")
    for acc in train_acc:
        print(acc)


    #print("Validation Accuracy:")
    #for acc in val_acc:
        #print(acc)

    #print("Predicted Labels:")
    for epoch, labels in enumerate(predicted_labels):
        #print(f"Epoch {epoch + 1}:")
        for digit in range(10):
            digit_average = labels[digit]
            #print(f"Digit {digit}: {', '.join(f'{value:.8f}' for value in digit_average)}")
        #print()


# Start training
app = QApplication(sys.argv)
#train()
load_trained_data("output.txt")


# Create the main window
main_window = MainWindow()
main_window.show()

sys.exit(app.exec())
